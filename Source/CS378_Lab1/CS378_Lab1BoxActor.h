// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "CS378_Lab1BoxActor.generated.h"

UCLASS()
class CS378_LAB1_API ACS378_Lab1BoxActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ACS378_Lab1BoxActor();


	FORCEINLINE class UStaticMeshComponent* GetMeshComponent() const { return MeshComponent;  }
	FORCEINLINE class UBoxComponent* GetTriggerComponent() const { return TriggerComponent; }

//protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UBoxComponent* TriggerComponent;

	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
		UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
		const FHitResult& SweepResult);
	
public:	

	// Called every frame
	virtual void Tick(float DeltaTime) override;


};
