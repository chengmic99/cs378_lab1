// Fill out your copyright notice in the Description page of Project Settings.


#include "CS378_Lab1BoxActor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"

// Sets default values
ACS378_Lab1BoxActor::ACS378_Lab1BoxActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	TriggerComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerComponent"));
	TriggerComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	//TriggerComponent->OnComponentBeginOverlap.AddDynamic(this, &ACS378_Lab1BoxActor::OnBeginOverlap);		// set up a notification for when this component hits something

}

// Called when the game starts or when spawned
void ACS378_Lab1BoxActor::BeginPlay()
{
	Super::BeginPlay();
	TriggerComponent->OnComponentBeginOverlap.AddDynamic(this, &ACS378_Lab1BoxActor::OnBeginOverlap);		// set up a notification for when this component hits something
}

void ACS378_Lab1BoxActor::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep,
	const FHitResult& SweepResult)
{
	if ((OtherActor != NULL) && (OtherActor != this) /*&& (OtherComp != NULL)*/)
	{
		Destroy();
		if(GEngine)
			GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Overlapping..."));
	}
}

// Called every frame
void ACS378_Lab1BoxActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

